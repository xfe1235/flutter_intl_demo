import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      onGenerateTitle: (context) {
        return AppLocalizations.of(context)!.helloWorld;
      },
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      localizationsDelegates: const [
        AppLocalizations.delegate, // Add this line
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: AppLocalizations.supportedLocales,
      home: const HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Localization Demo"),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Text(AppLocalizations.of(context)!.localeName),
            Text(AppLocalizations.of(context)!.pageHomeTitle("李四")),
            Text(AppLocalizations.of(context)!.pageHomeBirthday("male")),
            Text(AppLocalizations.of(context)!.pageHomeInboxCount(1)),
            Text(AppLocalizations.of(context)!.pageHomeBalance(20.333, DateTime.now())),
          ],
        ),
      ),
    );
  }
}
